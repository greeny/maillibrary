<?php

namespace Phoenix\Mail;

use Nette\Object;
use Iterator, Countable;

/**
 *
 * @author Tomáš Blatný
 */
class ContactGroup extends Object implements Iterator, Countable
{
    /** @var array of Contact */
    protected $contacts = array();

    /** @var int */
    protected $iterator = 0;

    /**
     * Creates contact group from data.
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach($data as $contact)
        {
            $this->contacts[] = new Contact($contact);
        }
    }

    /**
     * Returns all contacts string representations
     * @return string
     */
    public function __toString()
    {
        $return = "";
        foreach($this as $c)
        {
            $return .= $c.", ";
        }

        return substr($return, 0, -2);
    }

    //
    // Interface Countable
    //

    public function count()
    {
        return count($this->contacts);
    }

    //
    // Interface Iterator
    //

    public function current()
    {
        return $this->contacts[$this->iterator];
    }

    public function key()
    {
        return $this->iterator;
    }

    public function next()
    {
        $this->iterator++;
    }

    public function rewind()
    {
        $this->iterator = 0;
    }

    public function valid()
    {
        return isset($this->contacts[$this->iterator]);
    }
}
