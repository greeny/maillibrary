<?php

namespace Phoenix\Mail;
use Nette\Object;
use stdClass;

/**
 *
 * @author Tomáš Blatný
 */
class Addresses extends Object
{
    /** @var array of ContactGroup */
    protected $data = array();

    protected static $keys = array(
        "from" => "from",
        "to" => "to",
        "cc" => "cc",
        "bcc" => "bcc",
        "sender" => "sender",
        "replyTo" => "reply_to",
        "returnPath" => "return_path");

    public function __construct(stdClass $data)
    {
        foreach(self::$keys as $k => $v)
        {
            $data->$v = (isset($data->$v)?$data->$v:array());
            $this->addItem($k, $data->$v);
        }
    }

    protected function addItem($key, $value)
    {
        $this->data[$key] = new ContactGroup($value);
    }

    public function getItem($key)
    {
        return (isset($this->data[$key])?$this->data[$key]:null);
    }
}
