<?php

namespace Phoenix\Mail;

use Nette\Object;
use \Iterator, \Countable;

/**
 *
 * @author Tomáš Blatný
 */
class MailGetter extends Object implements Iterator, Countable
{
    /** @var resource */
    protected $connection;

    /** @var bool */
    protected $connected = false;

    /** @var array of int */
    protected $mailsIDs = array();

    /** @var array of Mail */
    protected $mails = array();

    /** @var int */
    protected $iterator = -1;

    /**
     * Connects to mail server.
     * @param string $username
     * @param string $password
     * @param string $host
     * @param string $port
     * @param bool $ssl activate SSL
     * @return MailGetter provides fluent interface
     * @throws MailException When connection fails or this function is called when connected.
     */
    public function connect($username, $password, $host, $port, $ssl = true)
    {
        if(!$this->connected)
        {
            $ssl = ($ssl == true) ? "/ssl" : '';
            $this->connection = imap_open("{".$host.":".$port."/imap".$ssl."}INBOX", $username, $password);
            if($this->connection)
            {
                $this->connected = true;
            }
            else
            {
                throw new MailException("Could not connect to mail server ".$host." at port ".$port.".");
            }
            return $this;
        }
        else
        {
            throw new MailException("Second call to MailGetter::connect() - already connected.");
        }
    }

    /**
     * Checks if connected to mail server.
     * @return bool
     */
    public function isConnected()
    {
        return (bool)$this->connected;
    }

    /**
     * Returns connection to mail server.
     * @return resource
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Gets mail ids from mail server. Internal function, you don't have to call it directly.
     * @return MailGetter provides fluent interface
     * @throws MailException when not connected
     */
    public function initialize()
    {
        if(!$this->mailsIDs)
        {
            if(!$this->connected)
            {
                throw new MailException("Call to MailGetter::getAllMails() when not connected.");
            }

            $mailIDs = imap_search($this->connection, "ALL");
            rsort($mailIDs);
            $this->mailsIDs = $mailIDs;
        }

        return $this;
    }

    /**
     * Gets email by id. Internal function, you don't have to call it directly.
     * @param $id
     * @return mixed
     * @throws MailException
     */
    protected function getMailById($id)
    {
        $this->initialize();
        if(isset($this->mails[$id]))
        {
            return $this->mails[$id];
        }
        else
        {
            if(isset($this->mailsIDs[$id]))
            {
                $this->mails[$id] = new Mail($this->mailsIDs[$id], $this->connection);
                return $this->mails[$id];
            }
            else
            {
                throw new MailException("Invalid email id (".$id.") in MailGetter::getMailById().");
            }
        }
    }

    /**
     * Returns next email from stack.
     * @return null|Mail email or null if email not found
     */
    public function getNext()
    {
        $this->next();
        if($this->valid())
        {
            return $this->current();
        }
        else
        {
            return null;
        }
        //return new Mail($this->mailsIDs[$this->iterator++], $this->connection);
    }

    /**
     * Resets iterator for getting emails.
     * @return MailGetter provides fluent interface
     */
    public function resetIterator()
    {
        $this->rewind();
        return $this;
    }

    //
    // Interface Iterator
    //

    public function current()
    {
        return $this->getMailById($this->iterator);
    }

    public function next()
    {
        $this->initialize();
        $this->iterator++;
    }

    public function rewind()
    {
        $this->initialize();
        $this->iterator = 0;
    }

    public function key()
    {
        return $this->iterator;
    }

    public function valid()
    {
        return isset($this->mailsIDs[$this->iterator]);
    }

    //
    // Interface Countable
    //

    public function count()
    {
        $this->initialize();
        return count($this->mailsIDs);
    }
}
