<?php

namespace Phoenix\Mail;

use Nette\Object;
use stdClass;

/**
 *
 * @author Tomáš Blatný
 */
class Contact extends Object
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $email;

    /**
     * Creates new contact
     * @param \stdClass $data data from imap_headerinfo().
     */
    public function __construct(stdClass $data)
    {
        $this->email = $email = $data->mailbox.'@'.$data->host;

        if(isset($data->personal))
        {
            $this->name = imap_utf8($data->personal);
        }
        else
        {
            $this->name = $data->mailbox;
        }
    }

    /**
     * Returns name.
     * @return string name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns email.
     * @return string email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns string representing this contact
     * @return string
     */
    public function __toString()
    {
        return $this->name." <".$this->email.">";
    }
}
