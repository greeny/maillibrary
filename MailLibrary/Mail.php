<?php

namespace Phoenix\Mail;

use Nette\Object;
use Exception;

/**
 *
 * @author Tomáš Blatný
 */
class Mail extends Object
{
    /** @var resource */
    protected $connection;

    /** @var int */
    protected $id;

    /** @var \stdClass */
    protected $data;

    /** @var \stdClass */
    protected $structure;

    /** @var Addresses */
    protected $addresses;

    protected static $mimeType = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

    /**
     * Creates new Mail
     * @param int $id id of email
     * @param resource $connection connection for getting details about email
     */
    public function __construct($id, $connection)
    {
        $this->connection = $connection;
        $this->id = $id;
    }

    /**
     * Initializes data of this mail. Internal function, you don't have to call it.
     * @return Mail provides fluent interface
     */
    protected function initialize()
    {
        if(!$this->data)
        {
            $this->data = imap_headerinfo($this->connection, $this->id);
            $this->addresses = new Addresses($this->data);
        }
        return $this;
    }

    /**
     * Returns id of this mail.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns connection resource to mail server.
     * @return resource
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Returns subject.
     * @return string
     */
    public function getSubject()
    {
        return imap_utf8($this->initialize()->data->subject);
    }

    public function getBody()
    {
        if(!$this->structure)
        {
            $this->structure = imap_fetchstructure($this->connection, $this->id);
        }

        if(($html = $this->getPart($this->id, "TEXT/HTML"))!="")
        {
            return $html;
        }
        else
        {
            return $this->getPart($this->id, "TEXT/PLAIN");
        }
    }

    /**
     * Returns timestamp when email was sent.
     * @return int
     */
    public function getTimestamp()
    {
        return $this->initialize()->data->udate;
    }

    /**
     * Returns unique identifier of this email.
     * @return string
     */
    public function getUniqueId()
    {
        return $this->initialize()->data->message_id;
    }

    /**
     * Returns from header.
     * @return ContactGroup
     */
    public function getFrom()
    {
        return $this->initialize()->addresses->getItem("from");
    }

    /**
     * Returns to header.
     * @return ContactGroup
     */
    public function getTo()
    {
        return $this->initialize()->addresses->getItem("to");
    }

    /**
     * Returns cc header.
     * @return ContactGroup
     */
    public function getCc()
    {
        return $this->initialize()->addresses->getItem("cc");
    }

    /**
     * Returns bcc header.
     * @return ContactGroup
     */
    public function getBcc()
    {
        return $this->initialize()->addresses->getItem("bcc");
    }

    /**
     * Returns reply to header.
     * @return ContactGroup
     */
    public function getReplyTo()
    {
        return $this->initialize()->addresses->getItem("replyTo");
    }

    /**
     * Returns sender header.
     * @return ContactGroup
     */
    public function getSender()
    {
        return $this->initialize()->addresses->getItem("sender");
    }

    /**
     * Returns return path header.
     * @return ContactGroup
     */
    public function getReturnPath()
    {
        return $this->initialize()->addresses->getItem("returnPath");
    }

    /**
     * Fet part of email
     * @param $mailId
     * @param $mime_type
     * @param $structure
     * @param $part_number
     * @return null|string
     */
    private function getPart($mailId, $mime_type, $structure = null, $part_number = null)
    {
        if(!$structure)
        {
            $structure = imap_fetchstructure($this->connection, $mailId);
        }
        if($structure)
        {
            if($mime_type == $this->getMimeType($structure))
            {
                if(!$part_number)
                {
                    $part_number = "1";
                }
                $text = imap_fetchbody($this->connection, $mailId, $part_number);
                if($structure->encoding == 3)
                {
                    return imap_base64($text);
                }
                else if($structure->encoding == 4)
                {
                    return imap_qprint($text);
                }
                else
                {
                    return $text;
                }
            }

            $prefix = '';
            if($structure->type == 1)
            {
                while(list($index, $sub_structure) = each($structure->parts))
                {
                    if($part_number)
                    {
                        $prefix = $part_number . '.';
                    }
                    $data = $this->getPart($this->connection, $mailId, $mime_type, $sub_structure,$prefix.($index + 1));
                    if($data)
                    {
                        return $data;
                    }
                }
            }
        }
        return null;
    }

    private function getMimeType($structure)
    {
        dump("Structure:", $structure);
        //throw new Exception;
        if($structure->subtype)
        {
            return self::$mimeType[(int) $structure->type] . '/' .$structure->subtype;
        }
        else
        {
            return "TEXT/PLAIN";
        }
    }
}